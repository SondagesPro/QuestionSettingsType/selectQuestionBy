<?php

/**
 * selectBySurveyQuestion get list of answers in another survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2025 Denis Chenu <www.sondages.pro>
 * @copyright 2017 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license GPL v3
 * @version 0.7.3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class selectQuestionBy extends PluginBase
{
    protected static $description = 'Use a text question as a dropdown filled by another question or a label';
    protected static $name = 'selectQuestionBy';
  /**
  * Add function to be used in beforeQuestionRender event and to attriubute
  */
    public function init()
    {
        $this->subscribe('beforeQuestionRender');
        $this->subscribe('newQuestionAttributes');

      /* Register package */
        Yii::setPathOfAlias('selectQuestionBy', dirname(__FILE__));
    }

  /**
   * The attribute
   */
    public function newQuestionAttributes()
    {
        $newAttributes = array(
            'selectQuestionByType' => array(
                'types' => 'T',
                'category' => $this->translate('Drop down'),
                'sortorder' => 100,
                'inputtype' => 'singleselect',
                'options' => array(
                    'none' => $this->translate("Disable"),
                    'question' => $this->translate("Question"),
                    'label' => $this->translate("Label"),
                ),
                'default' => 'none',
                'help' => $this->translate('Choose the type of source. Question must be a single choice in this survey. For label : an integer force a fixed label, string allow to choose the label by name.'),
                'caption' => $this->translate('Type of source'),
            ),
            'selectQuestionBySource' => array(
                'types' => 'T',/* Long text allow to save anything */
                'category' => $this->translate('Drop down'),
                'sortorder' => 110, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->translate('Question code in this survey, label id or name. With name : return the first find.'),
                'caption' => $this->translate('Value for the source'),
            ),
            'selectQuestionByDropdownType' => array(
                'types' => 'T',
                'category' => $this->translate('Drop down'),
                'sortorder' => 120,
                'inputtype' => 'singleselect',
                'options' => array(
                    'single' => $this->translate("Single"),
                    'multiple' => $this->translate("Multiple"),
                ),
                'default' => 'multiple',
                'help' => $this->translate('Choose if user can select multiple or only one choice'),
                'caption' => $this->translate('Dropdown type'),
            ),
            'selectQuestionByDropdownSystem' => array(
                'types' => 'T',
                'category' => $this->translate('Drop down'),
                'sortorder' => 130,
                'inputtype' => 'singleselect',
                'options' => array(
                    'select2' => $this->translate("Select2"),
                    'simple' => $this->translate("Simple"),
                    'multiselect' => $this->translate("Multiple select"),
                ),
                'default' => 'select2',
                'help' => $this->translate('Select2 allow user to search in the list, simple is the basi dropdown system.'),
                'caption' => $this->translate('Dropdown system'),
            ),
            'selectQuestionBySearch' => array(
                'types' => 'T',
                'category' => $this->translate('Drop down'),
                'sortorder' => 200,
                'inputtype' => 'switch',
                'default' => 1,
                'help' => $this->translate('Enable search for select2 and multiselect'),
                'caption' => $this->translate('Enable search'),
            ),
            'selectQuestionByClickableOptGroups' => array(
                'types' => 'T',
                'category' => $this->translate('Drop down'),
                'sortorder' => 210,
                'inputtype' => 'switch',
                'default' => 1,
                'help' => $this->translate('Enable category select all for multiselect (enableClickableOptGroups)'),
                'caption' => $this->translate('Enable select category'),
            ),
            'selectQuestionByIncludeSelectAllOption' => array(
                'types' => 'T',
                'category' => $this->translate('Drop down'),
                'sortorder' => 220,
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->translate('Select all option for multiselect (includeSelectAllOption)'),
                'caption' => $this->translate('Include select all option'),
            ),
            'selectQuestionByForceOne' => array(// Maybe add an automatic system for mandatory ?
                'types' => 'T',
                'category' => $this->translate('Drop down'),
                'sortorder' => 290,
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->translate('Only for select2 system'),
                'caption' => $this->translate('Disable removing last one'),
            ),
        );
        /* @todo : check if select2 is enable and remove it if not */
        if (!Yii::getPathOfAlias('BootstrapMultiselectPackage')) {
            unset($newAttributes['selectQuestionByDropdownSystem']['options']['multiselect']);
            /* @todo : hide specific settings */
        }
        if (method_exists($this->getEvent(), 'append')) {
            $this->getEvent()->append('questionAttributes', $newAttributes);
        } else {
            $questionAttributes = (array)$this->event->get('questionAttributes');
            $questionAttributes = array_merge($questionAttributes, $newAttributes);
            $this->event->set('questionAttributes', $questionAttributes);
        }
    }

    /**
    * Update Answer part in question display
    */
    public function beforeQuestionRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->getEvent();
        if ($oEvent->get('type') == 'T') {
            $aAttributes = \QuestionAttribute::model()->getQuestionAttributes($oEvent->get('qid'));
            $dataDropDownList = $this->getDataDropDownList($oEvent->get('surveyId'), $aAttributes['selectQuestionByType'], $aAttributes['selectQuestionBySource']);
            if (empty($dataDropDownList)) {
                return;
            }
            list($aListDropdown,$aListOptions) = $dataDropDownList;
            if (empty($aListDropdown)) {
                return;
            }
            $oQuestion = \Question::model()->find(
                "qid = :qid",
                array('qid' => $oEvent->get('qid'))
            );
            $name = $oEvent->get('surveyId') . 'X' . $oEvent->get('gid') . 'X' . $oEvent->get('qid');
            $actualValue = $_SESSION['survey_' . $oEvent->get('surveyId')][$name];
            $this->registerPackage();
            if ($aAttributes['selectQuestionByDropdownSystem'] == 'multiselect' && Yii::getPathOfAlias('BootstrapMultiselectPackage')) {
                \BootstrapMultiselectPackage\Utilities::registerScriptLanguage();
                if (method_exists('\BootstrapMultiselectPackage\Utilities', 'registerScriptTemplate')) {
                    \BootstrapMultiselectPackage\Utilities::registerScriptTemplate();
                }
            }
            switch ($aAttributes['selectQuestionByDropdownType']) {
                case 'single':
                    $empty = null;
                    if (!$actualValue) {
                        $empty = $this->translate("Please choose...");
                    } elseif ($oQuestion->mandatory != 'Y') {
                        $empty = $this->translate("(None)");
                    }
                    $answer = \CHtml::dropDownList(
                        $name,
                        $actualValue,
                        $aListDropdown,
                        array(
                            'id' => "answer{$name}",
                            'class' => 'form-control select-surveyquestion',
                            'prompt' => $empty,
                            'data-selectQuestionBy' => 'simple',
                            'data-selectQuestionSys' => $aAttributes['selectQuestionByDropdownSystem'],
                            'data-enablesearch' => $aAttributes['selectQuestionBySearch'],
                        )
                    );
                    $answer = \CHtml::tag("div", array('class' => 'answer-item select-item dropdown-item selectBySurveyQuestion-item'), $answer);
                    $oEvent->set('answers', $answer);
                    /* Create basic tip for choosing */
                    $tip = $this->getValidationTip($oQuestion->sid, $aAttributes);
                    $oEvent->set('valid_message', $tip . $oEvent->get('valid_message'));
                    Yii::app()->getClientScript()->registerPackage('selectQuestionBy');
                    break;
                case 'multiple':
                    $aActualValue = explode(',', $actualValue);
                    $dropDownHtml = \CHtml::dropDownList(
                        "select2-{$name}[]",
                        $aActualValue,
                        $aListDropdown,
                        array(
                            'id' => "select2-answer{$name}",
                            'class' => 'form-control select-surveyquestion',
                            'multiple' => true,
                            'data-update' => "answer{$name}",
                            'data-selectQuestionBy' => 'multiple',
                            'data-selectQuestionSys' => $aAttributes['selectQuestionByDropdownSystem'],
                            'data-enablesearch' => $aAttributes['selectQuestionBySearch'],
                            'data-clickableoptgroups' => $aAttributes['selectQuestionByClickableOptGroups'],
                            'data-includeselectalloption' => $aAttributes['selectQuestionByIncludeSelectAllOption'],
                            'data-forceone' => $aAttributes['selectQuestionByForceOne'],
                            'data-placeholder' => $this->translate("Select one or more options"),
                            'options' => $aListOptions
                        )
                    );
                    $dropDownHtml = \CHtml::tag("div", array('class' => 'answer-item select-multiple-item dropdown-multiple-item selectBySurveyQuestion-item'), $dropDownHtml);
                    $textareaHtml = \CHtml::textArea(
                        $name,
                        $actualValue,
                        array(
                            'id' => "answer{$name}",
                            'class' => 'form-control select-surveyquestion',
                        )
                    );
                    $textareaHtml = \CHtml::tag("div", array('class' => 'answer-item text-item hidden'), $textareaHtml);
                    $oEvent->set('answers', $dropDownHtml . $textareaHtml);
                    Yii::app()->getClientScript()->registerPackage('selectQuestionBy');
                    /* Create basic tip for choosing */
                    $tip = $this->getValidationTip($oQuestion->qid, $aAttributes);
                    $oEvent->set('valid_message', $tip . $oEvent->get('valid_message'));
                    break;
                default:
                  /* No update, @todo : log it */
            }
        }
    }

    /**
     * get tha validation tuip to help user to choose
     * @param $qid : question id
     * @param $aAttributes array of attroibnutes of question
     * @return string
     */
    private function getValidationTip($qid, $aAttributes)
    {
        $hideTip = $aAttributes['hide_tip'];
        $stringToParse = "";
        switch ($aAttributes['selectQuestionByDropdownType']) {
            case 'single':
                $tip = $this->translate('Choose one of the following answers');
                break;
            case 'multiple':
                switch ($aAttributes['selectQuestionByDropdownSystem']) {
                    case 'select2':
                        // Adding information for search ?
                        $tip = $this->translate('Select any of the following answers');
                        break;
                    case 'multiselect':
                        // Adding information for search ?
                        $tip = $this->translate('Select any of the following answers');
                        break;
                    case 'simple':
                        $tip = $this->translate('Select any of the following answers. Use CTRL Key to select multiple answers.');
                        break;
                }
                break;
        }

        $tipsDatas = array(
            'qid' => $qid,
            'coreId' => "vmsg_{$qid}_default", // If it's not this id : EM is broken
            'coreClass' => "ls-em-tip em_default",
            'vclass' => 'default',
            'vtip' => $tip,
            'hideTip' => $hideTip == 1,
            'qInfo' => [],
        );
        if (intval(App()->getConfig('versionnumber')) < 4) {
            $stringToParse = App()->getController()->renderPartial('//survey/questions/question_help/em-tip', $tipsDatas, true);
        } else {
            $stringToParse = App()->twigRenderer->renderPartial('/survey/questions/question_help/em_tip.twig', $tipsDatas);
        }
        // No need expression manager
        return $stringToParse;
    }

    /**
    * Get the dropdown list according to params
    * @param integer $sid current survey id
    * @param string $type type of source : can be question, label or survey
    * @param string $value value of the source
    * @return null|array
    */
    private function getDataDropDownList($sid, $type, $value)
    {
        $value = trim($value);
        if (empty($type) || $type == 'none' || empty($value)) {
            return;
        }
        switch ($type) {
            case 'question':
                return $this->getListByQuestion($sid, $value);
            case 'label':
                return $this->getListByLabel($value);
            default:
                return;
        }
    }

  /**
   * Get the dropdown list according to survey
   * @param integer $surveyid Question code
   * @param string $qcode Question code
   * @return array
   */
    private function getListByQuestion($surveyid, $qcode)
    {
        if (intval(App()->getConfig('versionnumber')) < 4) {
            return $this->getListByQuestionLegacy3LTS($surveyid, $qcode);
        }
        $language = Yii::app()->getLanguage();
        $oQuestion = Question::model()->find(
            "title=:title AND sid=:sid and (type='!' or type='L')",
            array(
                ":title" => $qcode,
                ":sid" => $surveyid,
            )
        );

        $aCategories = array();
        $aOptions = array();
        $aCategories = array();
        $aOptions = array();
        if ($oQuestion) {
            $oAnswers = Answer::model()->resetScope()->with('answerl10ns')->findAll(
                array(
                    'condition' => "qid=:qid and language = :language",
                    'order' => 'sortorder, code',
                    'params' => array(":qid" => $oQuestion->qid, ":language" => $language)
                )
            );
            if ($oAnswers) {
                /* Find if we need categories ? */
                $oSeparator = QuestionAttribute::model()->find("qid=:qid and attribute='category_separator'", array(":qid" => $oQuestion->qid));
                if (!$oSeparator || trim($oSeparator->value) === '') {
                    $aOptions = array();
                    foreach ($oAnswers as $oAnswer) {
                        if (is_null($oAnswer->answerl10ns[$language])) {
                            /* broken answer */
                            continue;
                        }
                        $answer = LimeExpressionManager::ProcessStepString($oAnswer->answerl10ns[$language]->answer, [], 3, 1);
                        if ($answer) {
                            $aOptions[$oAnswer->code] = $answer;
                        }
                    }
                    return [
                        $aOptions,
                        null
                    ];
                }
                $separator = trim($oSeparator->value);
                $aCategories = array();
                $aOptions = array();
                foreach ($oAnswers as $oAnswer) {
                    if (is_null($oAnswer->answerl10ns[$language])) {
                        /* broken answer */
                        continue;
                    }
                    $aAnswer = explode($separator, $oAnswer->answerl10ns[$language]->answer);
                    if (count($aAnswer) == 1) {
                        $answer = LimeExpressionManager::ProcessStepString($aAnswer[0], [], 3, 1);
                        if ($answer) {
                            $aCategories[$oAnswer->code] = $answer;
                            $aOptions[$oAnswer->code] = array();
                        }
                        continue;
                    }
                    $answer = LimeExpressionManager::ProcessStepString($aAnswer[1], [], 3, 1);
                    if ($answer) {
                        if (!isset($aCategories[$aAnswer[0]])) {
                            $aCategories[$aAnswer[0]] = array();
                        }
                        $aCategories[$aAnswer[0]][$oAnswer->code] = $answer;
                        $aOptions[$oAnswer->code] = array('data-group' => $aAnswer[0]);
                    }
                }
            }
        }
        if (count($aCategories) <= 1) {
            return array($aCategories,null);
        }
        return array($aCategories,$aOptions);
    }
  /**
   * Get the dropdown list according to survey
   * @param integer $surveyid Question code
   * @param string $qcode Question code
   * @return null|array
   */
    private function getListByQuestionLegacy3LTS($surveyid, $qcode)
    {
        $language = Yii::app()->getLanguage();
        $oQuestion = Question::model()->find(
            "title=:title AND sid=:sid and language=:language and (type='!' or type='L')",
            array(
                ":title" => $qcode,
                ":sid" => $surveyid,
                ":language" => $language
            )
        );
        $aCategories = array();
        $aOptions = array();
        if ($oQuestion) {
            $oAnswers = Answer::model()->findAll(array(
                'condition' => "qid=:qid and language=:language",
                'order' => "sortorder",
                'params' => array(':qid' => $oQuestion->qid,':language' => $oQuestion->language)
            ));
            if ($oAnswers) {
                /* Find if we need categories ? */
                $oSeparator = QuestionAttribute::model()->find("qid=:qid and attribute='category_separator'", array(":qid" => $oQuestion->qid));
                if (!$oSeparator) {
                    $aOptions = array();
                    foreach ($oAnswers as $oAnswer) {
                        $answer = LimeExpressionManager::ProcessStepString($oAnswer->answer, [], 3, 1);
                        if ($answer) {
                            $aOptions[$oAnswer->code] = $answer;
                        }
                    }
                    return [
                        $aOptions,
                        null
                    ];
                }
                $separator = trim($oSeparator->value);
                $aCategories = array();
                $aOptions = array();
                foreach ($oAnswers as $oAnswer) {
                    $aAnswer = explode($separator, $oAnswer->answer);
                    if (count($aAnswer) == 1) {
                        $answer = LimeExpressionManager::ProcessStepString($aAnswer[0], [], 3, 1);
                        if ($answer) {
                            $aCategories[$oAnswer->code] = $answer;
                            $aOptions[$oAnswer->code] = array();
                        }
                        continue;
                    }
                    $answer = LimeExpressionManager::ProcessStepString($aAnswer[1], [], 3, 1);
                    if ($answer) {
                        if (!isset($aCategories[$aAnswer[0]])) {
                            $aCategories[$aAnswer[0]] = array();
                        }
                        $aCategories[$aAnswer[0]][$oAnswer->code] = $answer;
                        $aOptions[$oAnswer->code] = array('data-group' => $aAnswer[0]);
                    }
                }
            }
        }
        return array($aCategories,$aOptions);
    }

  /**
   * Get the dropdown list according to survey
   * @param string $qcode Question code
   * @return null|array
   */
    private function getListByLabel($qcode)
    {
        if (intval(App()->getConfig('versionnumber')) < 4) {
            return $this->getListByLabelLegacy3LTS($qcode);
        }
        $language = Yii::app()->getLanguage();
        if (is_int($qcode)) {
            $oLabelSet = LabelSet::model()->findByPk($qcode);
        } else {
            $oLabelSet = LabelSet::model()->find("label_name = :code", array(":code" => $qcode));
        }
        if (empty($oLabelSet)) {
            return;
        }
        $allLanguage = explode(" ", trim($oLabelSet->languages));
        if (!in_array($language, $allLanguage)) {
            $language = App()->getConfig("defaultlang");
        }
        if (!in_array($language, $allLanguage)) {
            return;
        }
        $oLabels = Label::model()->resetScope()->with('labell10ns')->findAll(
            array(
                'condition' => "lid=:lid and language = :language",
                'order' => 'sortorder, code',
                'params' => array(":lid" => $oLabelSet->lid, ":language" => $language)
            )
        );
        if (!$oLabels) {
            return;
        }
        /* Find if we need categories ? */
        $separator = "|";
        $aCategories = array();
        $aOptions = array();
        $haveCategories = false;
        foreach ($oLabels as $oLabel) {
            if (is_null($oLabel->labell10ns[$language])) {
                /* broken answer */
                continue;
            }
            $aLabel = explode($separator, $oLabel->labell10ns[$language]->title);
            if (count($aLabel) == 1) {
                $aCategories[$oLabel->code] = $aLabel[0];
                $aOptions[$oLabel->code] = array();
                continue;
            }
            if (!isset($aCategories[$aLabel[0]])) {
                $aCategories[$aLabel[0]] = array();
            }
            $aCategories[$aLabel[0]][$oLabel->code] = $aLabel[1];
            $aOptions[$oLabel->code] = array('data-group' => $aLabel[0]);
        }
        return array($aCategories,$aOptions);
    }

  /**
   * Get the dropdown list according to survey
   * @param string $qcode Question code
   * @return null|array
   */
    private function getListByLabelLegacy3LTS($qcode)
    {
        $language = Yii::app()->getLanguage();
        if (is_int($qcode)) {
            $oLabelSet = LabelSet::model()->findByPk($qcode);
        } else {
            $oLabelSet = LabelSet::model()->find("label_name = :code", array(":code" => $qcode));
        }
        if (empty($oLabelSet)) {
            return;
        }
        $allLanguage = explode(" ", trim($oLabelSet->languages));
        if (!in_array($language, $allLanguage)) {
            $language = App()->getConfig("defaultlang");
        }
        if (!in_array($language, $allLanguage)) {
            return;
        }
        $oLabels = Label::model()->findAll(array(
            'condition' => "lid = :lid and language=:language",
            'order' => "sortorder",
            'params' => array(':lid' => $oLabelSet->lid, ':language' => $language)
        ));
        if (!$oLabels) {
            return;
        }
        /* Find if we need categories ? */
        $separator = "|";
        $aCategories = array();
        $aOptions = array();
        $haveCategories = false;
        foreach ($oLabels as $oLabel) {
            $aLabel = explode($separator, $oLabel->title);
            if (count($aLabel) == 1) {
                $aCategories[$oLabel->code] = $aLabel[0];
                $aOptions[$oLabel->code] = array();
                continue;
            }
            if (!isset($aCategories[$aLabel[0]])) {
                $aCategories[$aLabel[0]] = array();
            }
            $aCategories[$aLabel[0]][$oLabel->code] = $aLabel[1];
            $aOptions[$oLabel->code] = array('data-group' => $aLabel[0]);
        }
        return array($aCategories,$aOptions);
    }

    /**
    * Done just after plugin loaded, allow to update Yii and LS config
    */
    public function registerPackage()
    {
        if (Yii::app()->clientScript->hasPackage('selectQuestionBy')) {
            return;
        }
        $depends = [];
        /* 5.X didn't work : need plugin */
        if (Yii::app()->clientScript->hasPackage('select2-bootstrap-theme')) {
            $depends[] = 'select2-bootstrap-theme';
        }
        if (Yii::app()->clientScript->hasPackage('bootstrap-multiselect-public')) {
            $depends[] = 'bootstrap-multiselect-public';
        }
        Yii::app()->clientScript->addPackage('selectQuestionBy', array(
            'basePath' => 'selectQuestionBy.assets',
            'css' => array('selectQuestionBy.css'),
            'js' => array('selectQuestionBy.js'),
            'depends' => $depends,
        ));
    }

    /**
     * shortcut for parent->gt
     * @param string
     * @return string
     */
    private function translate($string)
    {
        return parent::gT($string, 'unescaped');
    }
}
