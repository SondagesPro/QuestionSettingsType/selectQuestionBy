/**
 * @author Denis Chenu
 * @copyright Denis Chenu <http://www.sondages.pro>
 * @version 0.7.0
 * @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat (MIT)
 */
/* jshint esversion: 6 */
$(function () {
  $("[data-selectQuestionBy='simple'][data-selectQuestionSys='select2']").each(function() {
    let options = {
      minimumResultsForSearch: $(this).data("enablesearch") ? 1 : Infinity,
      theme: "bootstrap"
    };
    $(this).select2(options);
  });
  $("[data-selectQuestionBy='simple'][data-selectQuestionSys='multiselect']").each(function(){
    let options = {
      enableCaseInsensitiveFiltering : true,
      enableFiltering: $(this).data("enablesearch"),
    };
    $(this).multiselect(options);
  });
  $("[data-selectQuestionBy='multiple'][data-selectQuestionSys='select2']").each(function() {
    let options = {
      minimumResultsForSearch: $(this).data("enablesearch") ? 1 : Infinity,
      theme: "bootstrap",
      //~ dropdownParent: $('.answer-item'),
      escapeMarkup: function (markup) { return markup; },
      templateSelection: function (data) {
        if (data.element && $(data.element).data("group")) {
          return "<strong>[" + $(data.element).data("group") + "]</strong> " + data.text;
        }
        return data.text;
      },
      templateResult: function (data) {
        return data.text;
      }
    };
    $(this).select2(options);
  });
  $("[data-selectQuestionBy='multiple'][data-selectQuestionSys='multiselect']").each(function() {
    let options = {
      enableCaseInsensitiveFiltering : true,
      enableFiltering: $(this).data("enablesearch"),
      enableClickableOptGroups: $(this).data("clickableoptgroups"),
      includeSelectAllOption: $(this).data("includeselectalloption"),
    };
    $(this).multiselect(options);
  });


  $("[data-selectQuestionBy='multiple']").on("change", function (e) {
    if ($(this).val()) {
      $("#" + $(this).data('update')).val($(this).val().join(',')).trigger("keyup");
    } else {
      $("#" + $(this).data('update')).val("").trigger("keyup");
    }
  });
});
$(document).on("select2:unselecting", "[data-selectQuestionBy='multiple'][data-forceone=1]", function (e) {
  if ($(this).val().length < 2) {
    e.preventDefault();
    return false;
  }
});
