# selectQuestionBy

Allow to choose a question or a label to create dropdown with multiple choice or not. Can use select2 or simple dropddown.

This allow to get big multiple choice question without one colun for each choice.

Warning

- This plugin need [select2package](https://gitlab.com/SondagesPro/coreAndTools/select2package) on LimeSurey 5X and up
- Javascript need to be activated for dropdown with multiple choice
- When update from 0.4 and previous version, need to reinstall to set priority

## Home page & Copyright
- HomePage <https://sondages.pro/>
- Copyright © 2022-2024 Denis Chenu <https://sondages.pro>
- [Support](https://support.sondages.pro)
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 

Distributed under [GNU AFFERO GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/agpl.txt) licence
